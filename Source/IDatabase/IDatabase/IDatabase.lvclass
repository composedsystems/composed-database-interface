﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="EndevoGOOP_ClassBodyIcon" Type="Str">Interface</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">3633920</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">IDatabase.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../IDatabase.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+[!!!*Q(C=T:1^DB."%%;`!33Q2/+-%$EB2;IL7/)%TJ'1;C5#5K&gt;?#0!6[A374-!"H##%C%K%THQ&amp;=Y4B45^\\@W22SN!WO[NW?GPKKP?&gt;,&gt;&lt;+OW:^%4N5&amp;O&gt;&lt;@CF&amp;&gt;9^6KMS/,RU\ZV`V@NU?03^$TPV^YG/S=\.L].$`;0`%(/=0]!`V(3WX=^`/_(JM'V(8&lt;M7=2R5\YW)%X^RN#8M$H]\/P'0WLP^I`&lt;!=.0@DK\ZLQ)?CH_)@`$\B^:P=0W(^K]@H&gt;P`O^L@_Y?0^W,Y2`"`G^155CSRQ"ST]H6NIC&gt;[IC&gt;[IC&gt;[I!&gt;[I!&gt;[I!?[ITO[ITO[ITO[I2O[I2O[I2N[[?B#&amp;\L17:7E?&amp;)I3:IE3)*"5@*)?"+?B#@B9;C%*_&amp;*?"+?B)=1*4Q*4]+4]#1]4&amp;0#E`!E0!F0QE/K1J+FI]/4]*"?!5`!%`!%0!%0*28Q"!""M3"RE!1-"=\A*?!*?!)?8B8Q"$Q"4]!4]/"7Q"0Q"$Q"4]$$F,)K57C[DAY0;?4Q/$Q/D]0D]*";$I`$Y`!Y0!Y0Z?4Q/$Q/B&amp;0131[#H%F/A$.Q?"Q?`MHB=8A=(I@(Y=&amp;6&gt;MD,SH1U85?(R_!R?!Q?A]@A)95-(I0(Y$&amp;Y$"\3SO!R?!Q?A]@AI:1-(I0(Y$&amp;!D++5FZ(-G'A%'9,"QV]Z,6:W+1K*F6Z`GPV"64W!KA&gt;,^=#I(A46$6&lt;&gt;/.5.56VIV16582D6$V&lt;^%&amp;6!V=+K#65$N??ZQT&lt;9'FNC=WS'4&lt;%*.O[G`O0!`8[PX7[HT7;D^8KNZ8+J_8SOW7SG[83KS73C]8B]&gt;6O^J0?N/&lt;W8]M0]V8;\`LX^_?ZCO`XS^&gt;@W`=8"NS#WW..PT?,&amp;[W&lt;R^HGT?0/D78T_XCQ_@7QO(V`?MEYP`C[OC_`G&gt;@-0O&lt;$_8HI)&gt;[-?K4WZZFGD0]1)^(M!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5_0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.D%U.$QP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-4=Q0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$%],UZB&lt;75_$1I]6G&amp;M0DAV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D%X-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!T0#^/97VF0AU+0&amp;:B&lt;$YY.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YR.T!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_/$5],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-4=Q0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$=],UZB&lt;75_$1I]6G&amp;M0DAV0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YR0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D)U-D9Y/$QP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_-49X.T=S-45],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D-],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"6;5F.31QU+!!.-6E.$4%*76Q!!%/1!!!2W!!!!)!!!%-1!!!!G!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!!!!!!+!A!)!!!$!!!!A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!\0SW93-#*5'V60TV(B]'&gt;Q!!!!Q!!!!1!!!!!I=`#V15H?2.LU9%KYSFY]05(9T:DQ#S"/G!#:DM_%*_!!!!!!!!!!#N5JHS#&gt;*"3+.*+S%TVGR;!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%/SDX&amp;@!*VJ^E1$Z'C_??+U!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!C!!!!((C=9W"D9'JAO-!!R)R!T.4!^!0)`A$C-Q!!;!%).A!!!!!!21!!!2BYH'.AQ!4`A1")-4)Q-*U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"SK8J"`Q(Z#!7R9T!9!&gt;P5I&amp;1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!&amp;$!!!#H(C==W"E9-AUND"L!.,-1+T!U-#1H*_3SM5!Z$.!1!M4!]5A!'K?&amp;JKYY9($;5#ARS`@!O:XO[CQ..?I]$#6]PUP5?%)?!%3&lt;$\#=&lt;D&lt;)_?YIQV9#5=71R:$Q0`!D/9D0'$&gt;S0J^6&amp;A-$T25+D/5#B^P.''%W")).9&lt;F]![1S'&amp;5_Y"[/!Y_:/G&gt;#"&lt;I"*'&gt;)9Q3RVU9&gt;=3!\&amp;Y?RE#%/^$M#_M'OD+,-1QGX]VWX%%$R$\O)!+B-C"5"91K!&amp;%\1%4=92#Z^P7^8;"Q95-+%Q=I&lt;A"B2A479W"E!$G@#1B"JP\Z``_`$6#%#3KG#"5$M6&gt;#W2J)?EZ#R2S1\!(J":H1#[1VI/T*5(9$V&amp;UAM;N!OA$+PA/.4R$\-61&gt;C0U+3!N!W2_"^!%I_RO5X1#.$84;W&gt;`&amp;&amp;4F.Q.)&lt;!0_5;J!!!!!!$#!!A"1!!!1S-#YQ!!!!!!QA!)!!!!!%-D!O-!!!!!!-)!#!&amp;!!!"$)Q,D!!!!!!$#!!A!!!!!1S-#YQ!!!!!!QA!)!5!!!%-D!O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$``````````]RO&gt;S@7VKLPVM:D:^&lt;7K[`/VGJH``````````_!!!!"A!!!!9!(Q!'!'$!"A'!-!9"(R!'!B])"A)/#!9%$A1'"!Y%"A1/"!9%$A1'"!Y%"A)/#!9#(QA'!2]1"A'!-!9!9-!'!"]!"A!!!!9!!!!'!!!!"`````Q!!!A$`````````````````````_:G:G:G:G:G:G:G:G:G:H`E!G1!*E*G1#:E*G1#1#:`Z#1G1G1E*E*#1E*#:E*G@_1E*E*E!#:!*E!#:#:!*H`E*#:#:#1G1E*#1G:#1G:`Z!*G1G1E*E!G1E*!*E!G@_:G:G:G:G:G:G:G:G:G:H``````````````````````Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!```Q!!!!!!!!`Q!!!!!!`Q!!$`!!!!!!!0]!!!!!`Q!!!!!0]!!!!!$`!!!!!0!!```Q!0!!!!!!`Q!!!!]!!0``]!!0!!!!!0]!!!!0!!!0`Q!!$Q!!!!$`!!!!]!!!$`]!!!$Q!!!!`Q!!!0!!!!``!!!!]!!!!0]!!!$Q!!!0`Q!!!0!!!!$`!!!!]!!!$`]!!!$Q!!!!`Q!!!0!!!!``!!!!]!!!!0]!!!!0!!!0`Q!!$Q!!!!$`!!!!$Q!!```Q!!]!!!!!`Q!!!!$Q!0``]!$Q!!!!!0]!!!!!`Q!!!!!0]!!!!!$`!!!!!!$`!!!0]!!!!!!!`Q!!!!!!!0``]!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````_HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[@``[=!!+?H!!!!J[=!J[?H!!#HJ[=!J[?H!!#H!!#HJ```JQ#H!+?H!+?H!+=!J[=!JQ#H!+=!JQ#HJ[=!J[?H``_H!+=!J[=!J[=!!!#HJQ!!J[=!!!#HJQ#HJQ!!J[@``[=!JQ#HJQ#HJQ#H!+?H!+=!JQ#H!+?HJQ#H!+?HJ```JQ!!J[?H!+?H!+=!J[=!!+?H!+=!JQ!!J[=!!+?H``_HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[?HJ[@`````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!0``````!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!0``#1E*#1H``Q!!!!!!!!!!!!!!``]!!!!!!!!!!0``#1E*#1E*#1E*``]!!!!!!!!!!!$``Q!!!!!!!!!!`QE*#@``````#1E*`Q!!!!!!!!!!!0``!!!!!!!!!0]*#1E*``````]*#1E*`Q!!!!!!!!!!``]!!!!!!!!!`QE*#1E*````#1E*#1H`!!!!!!!!!!$``Q!!!!!!!0]*#1E*#1H```]*#1E*#1H`!!!!!!!!!0``!!!!!!!!`QE*#1E*#@```QE*#1E*#@]!!!!!!!!!``]!!!!!!!$`#1E*#1E*````#1E*#1E*`Q!!!!!!!!$``Q!!!!!!!0]*#1E*#1H```]*#1E*#1H`!!!!!!!!!0``!!!!!!!!`QE*#1E*#@```QE*#1E*#@]!!!!!!!!!``]!!!!!!!!!`QE*#1E*````#1E*#1H`!!!!!!!!!!$``Q!!!!!!!!$`#1E*#@``````#1E*#@]!!!!!!!!!!0``!!!!!!!!!!$`#1E*``````]*#1H`!!!!!!!!!!!!``]!!!!!!!!!!0``#1E*#1E*#1E*``]!!!!!!!!!!!$``Q!!!!!!!!!!!!$``QE*#1E*``]!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!``````]!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!!1!!!!!!!!#C!!!"/^YH+W50WA452T(@S^=SUNI^6VM&lt;1-.C?5;CRA12;X"0]7_#J6;F&amp;A1'D$2CX]A.:I`YN1OBZ!BCY%-9J=-'6Q=-LB,=,F"JQZWC-XKYF*5\.XZOWPP,EER,C&lt;Q/-,\@,`P@N^P(I$QD9V\&gt;&amp;D4A,!&gt;@&amp;D5Q#?L"+!:J&lt;$`C&lt;Q%NER_!RE*%!VG[4,&lt;]OBE1I-B79X15\Q%XX'X]&gt;5I1Q6_M7X=/MA#+/&lt;4Y,#MDIH8J2;4XEV)J1&amp;&lt;691A+R0&gt;=U=+`;"F*9O'I%S&lt;KRAF/B"_4"#5S:OJ*WF&amp;-H`V2GH!EP2KQ,A[H*.;RV%2L4^9EG1&amp;;C2D3Q*+4E/DU8!B=1_+7-?9-:E;SD&lt;)3B`'T^6A8GK&gt;N"C@R;"0Q`&lt;B)&lt;VCHNW%?N%D8'7))P@#@O7;\=6$WKT&amp;N&gt;NNZ($&gt;ZRZJ-#+VLN)!X:;`$!T@5NY$!;*]BE%YV,R,D5XD,&lt;QB.]1F-QO,]'-=5T(#YPA=V_#%IHK34$$$U.%R$R5L$-%/Y[):RD5L$#]P-8$4K0QD$3YU*Y.TG7+_E-[&amp;MQ`#^T/J@$\].0@Y?;K1$MOJ1OJA4J?Y'DBDTM!UMSI#IS$!-UBWTDQ,^8I&gt;RY#LCVZ'&gt;%RK/:S)UE71H&lt;H,\NR.6X&gt;_6X"_`/@NB_9-JW)?O\HA&gt;P9U&gt;F:EP1@K[OT:`^`:=VCFN:\/1A+`KXX[&gt;XY0[OBM!KF6302B:J":\_KMZ7,\^/PMB9/&gt;4&gt;B?4G?LV7I8BT/0/:U6#,%\;_Q;O^B:L+Y!^S"J&gt;2;]3RUHJVM@C]Y61_-3.WDA\RP90*KN;T$+Z`S@W!+K$&gt;%&amp;/M]W-&amp;7]F)\3$&lt;L4X(1OI?;C1_,`JU*?3;^$&gt;0Q0^J&amp;MMA!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!"!!!!!!!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!#!Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!0#!!A!!!!!!"!!A!-0````]!!1!!!!!!)!!!!!%!'%"1!!!2352B&gt;'&amp;C98.F,GRW9WRB=X-!!1!!!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#5A!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"EA!)!!!!!!!1!&amp;!!=!!!%!!.BO&lt;:!!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!W'ZNE!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!!_)!#!!!!!!!%!#!!Q`````Q!"!!!!!!!C!!!!!1!;1&amp;!!!".*2'&amp;U97*B=W5[352B&gt;'&amp;C98.F!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'3!!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6)!#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.]!!!&amp;D?*S.DUN/!E%9B,_G56YCLT6*,VSY=O-&amp;*N'9O#/?A);:)33N9ZC'O01)(M^D[!UM"AAG&lt;%AF@\KK[[`K"E:-_@V_?`U#&lt;/`ZU5=`]W6W&amp;T:B/2P]Z`0AS\*\6/9R=*N-90Q1VG8-6K\)876S\[PFRM@-J@*S)&gt;!G"@/$%&lt;&amp;\SLW/L;3"T=/#:P*JCZ?Y%\)CJ[^M+`MF$&lt;N/=^L&lt;,BM`5GV0K.'ER=V:`47K'!W$VK;KLP/E%-=689F&lt;8+PM!,0(K8+][689@=416S*[%#KL3_UQ9+D:_10UYDJ9!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$:!.1!!!"2!!]%!!!!!!]!W1$5!!!!7A!0"!!!!!!0!.E!V!!!!'/!!)1!A!!!$Q$:!.1)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"$E!!!%&gt;A!!!#!!!"$%!!!!!!!!!!!!!!!A!!!!.!!!"'A!!!!?4%F#4A!!!!!!!!&amp;Y4&amp;:45A!!!!!!!!'-5F242Q!!!!!!!!'A1U.46!!!!!!!!!'U4%FW;1!!!!!!!!()1U^/5!!!!!!!!!(=6%UY-!!!!!!!!!(Q2%:%5Q!!!!!!!!)%4%FE=Q!!!!!!!!)96EF$2!!!!!!!!!)M&gt;G6S=Q!!!!1!!!*!5U.45A!!!!!!!!+E2U.15A!!!!!!!!+Y35.04A!!!!!!!!,-;7.M.!!!!!!!!!,A;7.M/!!!!!!!!!,U4%FG=!!!!!!!!!-)2F"&amp;?!!!!!!!!!-=2F")9A!!!!!!!!-Q2F"421!!!!!!!!.%6F"%5!!!!!!!!!.94%FC:!!!!!!!!!.M1E2&amp;?!!!!!!!!!/!1E2)9A!!!!!!!!/51E2421!!!!!!!!/I6EF55Q!!!!!!!!/]2&amp;2)5!!!!!!!!!01466*2!!!!!!!!!0E3%F46!!!!!!!!!0Y6E.55!!!!!!!!!1-2F2"1A!!!!!!!!1A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!M!!!!!!!!!!$`````!!!!!!!!!.!!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!$M!!!!!!!!!!$`````!!!!!!!!!2A!!!!!!!!!!0````]!!!!!!!!")!!!!!!!!!!!`````Q!!!!!!!!&amp;)!!!!!!!!!!$`````!!!!!!!!!:1!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!%`````Q!!!!!!!!,M!!!!!!!!!!@`````!!!!!!!!!PQ!!!!!!!!!#0````]!!!!!!!!$$!!!!!!!!!!*`````Q!!!!!!!!-=!!!!!!!!!!L`````!!!!!!!!!SQ!!!!!!!!!!0````]!!!!!!!!$0!!!!!!!!!!!`````Q!!!!!!!!.5!!!!!!!!!!$`````!!!!!!!!!WA!!!!!!!!!!0````]!!!!!!!!$\!!!!!!!!!!!`````Q!!!!!!!!8Q!!!!!!!!!!$`````!!!!!!!!#@1!!!!!!!!!!0````]!!!!!!!!+"!!!!!!!!!!!`````Q!!!!!!!!I-!!!!!!!!!!$`````!!!!!!!!$*A!!!!!!!!!!0````]!!!!!!!!-I!!!!!!!!!!!`````Q!!!!!!!!SI!!!!!!!!!!$`````!!!!!!!!$,A!!!!!!!!!!0````]!!!!!!!!-Q!!!!!!!!!!!`````Q!!!!!!!!UI!!!!!!!!!!$`````!!!!!!!!$4!!!!!!!!!!!0````]!!!!!!!!0/!!!!!!!!!!!`````Q!!!!!!!!^!!!!!!!!!!!$`````!!!!!!!!$UA!!!!!!!!!!0````]!!!!!!!!0&gt;!!!!!!!!!#!`````Q!!!!!!!""9!!!!!!V*2'&amp;U97*B=W5O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!!!!1!!1!!!!!!!!%!!!!"!"B!5!!!%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!%!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!0``!!!!!1!!!!!!!1%!!!!"!"B!5!!!%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!%!!!!!!!(````_!!!!!!!!!AR*47^E:7QO&lt;(:M;7)/35VP:'6M,GRW9WRB=X.16%AQ!!!!!!!!!!!!'!#!!!!!!!!!!!$``Q!!!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!)-35VP:'6M,GRW&lt;'FC$EF.&lt;W2F&lt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!``]!!!!"!!!!!!!"!!!!!!%!'%"1!!!2352B&gt;'&amp;C98.F,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!1!!!"&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="IDatabase.ctl" Type="Class Private Data" URL="IDatabase.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Connection" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Open Connection.vi" Type="VI" URL="../Open Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]21W^O&lt;G6D&gt;'FP&lt;C"T&gt;(*J&lt;G=!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234192</Property>
		</Item>
		<Item Name="Close Connection.vi" Type="VI" URL="../Close Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!-352B&gt;'&amp;C98.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">20971648</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
	</Item>
	<Item Name="Query" Type="Folder">
		<Item Name="Types" Type="Folder">
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Double" Type="Folder">
				<Item Name="Query with 2D Double Result.vi" Type="VI" URL="../Query with 2D Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 1D Double Result.vi" Type="VI" URL="../Query with 1D Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;I!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with Scalar Double Result.vi" Type="VI" URL="../Query with Scalar Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
			</Item>
			<Item Name="String" Type="Folder">
				<Item Name="Query with Scalar String Result.vi" Type="VI" URL="../Query with Scalar String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 1D String Result.vi" Type="VI" URL="../Query with 1D String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!51%!!!@````]!"1:3:8.V&lt;(1!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 2D String Result.vi" Type="VI" URL="../Query with 2D String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!P``````````!!5'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
			</Item>
			<Item Name="Int32" Type="Folder">
				<Item Name="Query with 2D Integer Result.vi" Type="VI" URL="../Query with 2D Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 1D Integer Result.vi" Type="VI" URL="../Query with 1D Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;I!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with Scalar Integer Result.vi" Type="VI" URL="../Query with Scalar Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
			</Item>
			<Item Name="Int64" Type="Folder">
				<Item Name="Query with Scalar 64bit Integer Result.vi" Type="VI" URL="../Query with Scalar 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 1D 64bit Integer Result.vi" Type="VI" URL="../Query with 1D 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;I!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!51%!!!@````]!"1:3:8.V&lt;(1!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with 2D 64bit Integer Result.vi" Type="VI" URL="../Query with 2D 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!91%!!!P``````````!!5'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1073741984</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
			</Item>
			<Item Name="Query with no type.vi" Type="VI" URL="../Query with no type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!5Q&gt;798*J97ZU!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!J!5Q25?8"F!!!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8400912</Property>
			</Item>
		</Item>
		<Item Name="Query.vim" Type="VI" URL="../Query.vim">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!81!I!%&amp;2Z='5A97ZE)%2F:G&amp;V&lt;(1!!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!U*!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!3!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8389648</Property>
		</Item>
	</Item>
	<Item Name="List" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="List Columns.vi" Type="VI" URL="../List Columns.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!"B!1!!"`````Q!&amp;#U.P&lt;(6N&lt;H-A&lt;X6U!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!?1%!!!P``````````!!5-1W^M&gt;7VO=S"J&lt;G:P!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!)!!1!"!!%!!E!"!!+!!M#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!#1!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">9187328</Property>
		</Item>
		<Item Name="List Databases.vi" Type="VI" URL="../List Databases.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!":!1!!"`````Q!&amp;#72B&gt;'&amp;C98.F=Q![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"U.P&lt;H2F?(1!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
		<Item Name="List Tables.vi" Type="VI" URL="../List Tables.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!"2!1!!"`````Q!&amp;"H2B9GRF=Q!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!V*2'&amp;U97*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QBE982B9G&amp;T:1!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
	</Item>
	<Item Name="Statements" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Prepare Statement.vi" Type="VI" URL="../Prepare Statement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"F!#!!45X2B&gt;'6N:7ZU)'FO:'6Y)'^V&gt;!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;.U982F&lt;76O&gt;#"J&lt;A!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
		<Item Name="Execute Statement.vi" Type="VI" URL="../Execute Statement.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'I!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````].5'&amp;S97VF&gt;'6S=S"J&lt;A!91%!!!P``````````!!5'5G6T&gt;7RU!!![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$5F%982B9G&amp;T:3"P&gt;81!&amp;E"!!!(`````!!5*6G&amp;M&gt;76T)'FO!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'E"!!!(`````!!5.5'&amp;S97VF&gt;'6S=S"J&lt;A!:1!A!%F.U982F&lt;76O&gt;#"J&lt;G2F?#"J&lt;A!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!A!"!!*!!I!#Q!-!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!##!!!!!!!!!!)!!!##!!!!!A!!!#1!!!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
	</Item>
	<Item Name="Metadata" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Metadata - database.vi" Type="VI" URL="../Metadata - database.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;X!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!C1%!!!P``````````!!522'&amp;U97*B=W5A&lt;76U972B&gt;'%!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!V*2'&amp;U97*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%!Q`````QN%982B9G&amp;T:3"J&lt;A![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
		<Item Name="Metadata - table.vi" Type="VI" URL="../Metadata - table.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;V!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!A1%!!!P``````````!!5/6'&amp;C&lt;'5A&lt;76U972B&gt;'%!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````],2'&amp;U97*B=W5A;7Y!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269234176</Property>
		</Item>
	</Item>
	<Item Name="Stored Procedure" Type="Folder">
		<Item Name="invoke stored procedure.vi" Type="VI" URL="../stored procedure/invoke stored procedure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!(Z!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E5=X2P=G6E)("S&lt;W.F:(6S:3"P&gt;81!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]1=X2P=G6E)("S&lt;W.F:(6S:1!!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="add input param stored procedure.vi" Type="VI" URL="../stored procedure/add input param stored procedure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*I!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!(Z!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E5=X2P=G6E)("S&lt;W.F:(6S:3"P&gt;81!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!91$$`````$X"B=G&amp;N:82F=C"W97RV:1!91$$`````$H"B=G&amp;N:82F=C"O97VF!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!!Q!':'*5?8"F!!"]1(!!'!!!!!%!!!";5XFT&gt;'6N,E^C;G6D&gt;#QA&lt;8.D&lt;X*M;7)M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVC.T&gt;B.7-V.D%Z-T2F-$AZ%X.U&lt;X*F:#"Q=G^D:72V=G5A;7Y!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!%!!A!"!!*!!I!#Q!-!A!!?!!!$1A!!!!!!!!*!!!!D1M!!AA!!!!!!!!##!!!!!!!!!!)!!!!#!!!!!A!!!#1!!!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
		</Item>
		<Item Name="add output param stored procedure.vi" Type="VI" URL="../stored procedure/add output param stored procedure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*9!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!(Z!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E5=X2P=G6E)("S&lt;W.F:(6S:3"P&gt;81!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!61!-!$H"B=G&amp;N:82F=C"U?8"F!!!91$$`````$H"B=G&amp;N:82F=C"O97VF!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!(R!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E4=X2P=G6E)("S&lt;W.F:(6S:3"J&lt;A![1(!!(A!!)Q^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X-!$%F%982B9G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!(!!1!#!!%!!E!"!!+!!M#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!#!!!!!!!!!))!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="execute stored procedure.vi" Type="VI" URL="../stored procedure/execute stored procedure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Y!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!$!!:S:8.V&lt;(1!!(Z!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E5=X2P=G6E)("S&lt;W.F:(6S:3"P&gt;81!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"]1(!!'!!!!!%!!!";5XFT&gt;'6N,E^C;G6D&gt;#QA&lt;8.D&lt;X*M;7)M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVC.T&gt;B.7-V.D%Z-T2F-$AZ%X.U&lt;X*F:#"Q=G^D:72V=G5A;7Y!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!#1!+!A!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="get output stored procedure.vi" Type="VI" URL="../stored procedure/get output stored procedure.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*.!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!#E"4"8:B&lt;(6F!(Z!=!!9!!!!!1!!!&amp;J4?8.U:7UO4W*K:7.U,#"N=W.P=GRJ9CQA6G6S=WFP&lt;DUQ,D!O-#YQ,#"$&gt;7RU&gt;8*F07ZF&gt;82S97QM)&amp;"V9GRJ9UNF?62P;W6O07)X.W%V9T5W-4ET.'5Q/$E5=X2P=G6E)("S&lt;W.F:(6S:3"P&gt;81!!$J!=!!?!!!D$UF%982B9G&amp;T:3ZM&gt;GRJ9B&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!.352B&gt;'&amp;C98.F)'^V&gt;!!%!!!!'%!Q`````QZQ98*B&lt;76U:8)A&lt;G&amp;N:1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"]1(!!'!!!!!%!!!";5XFT&gt;'6N,E^C;G6D&gt;#QA&lt;8.D&lt;X*M;7)M)&amp;:F=H.J&lt;WY^-#YQ,D!O-#QA1X6M&gt;(6S:4VO:86U=G&amp;M,#"1&gt;7*M;7.,:8F5&lt;WNF&lt;DVC.T&gt;B.7-V.D%Z-T2F-$AZ%X.U&lt;X*F:#"Q=G^D:72V=G5A;7Y!/E"Q!"Y!!#-0352B&gt;'&amp;C98.F,GRW&lt;'FC%5F%982B9G&amp;T:3ZM&gt;G.M98.T!!R*2'&amp;U97*B=W5A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!A!"Q!*!!=!#A!,!A!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!##!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
	</Item>
</LVClass>
